import { NowRequest, NowResponse } from '@now/node'
const express = require('express')
const app = express()

app.get('/', function(req, res) {
  res.send('GET homepage.')
})

app.post('/teams', function(req, res) {
  res.send('POST teams data.')
})

app.get('/teams/data', function(req: NowRequest, res: NowResponse) {
    
    const { name = 'World' } = req.query
    res.send(`Hello ${name}!`)
  })




