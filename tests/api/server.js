const express = require('express')
const app = express()


app.get('/', function(req, res) {
  res.send('GET homepage.')
})

app.post('/teams', function(req, res) {
  res.send('POST teams data.')
})

app.get('/teams/data', function(req, res) {
  res.send('GET teams data.')
})